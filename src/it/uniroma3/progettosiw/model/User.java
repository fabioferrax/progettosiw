package it.uniroma3.progettosiw.model;

import java.util.List;

import it.uniroma3.progettosiw.helper.Role;

public class User {
	private String username;
	private String password;
	private Role role;
	private List<Opera> favoriteWorks;
	
	public User(){
		
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	

}
