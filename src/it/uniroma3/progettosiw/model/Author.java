package it.uniroma3.progettosiw.model;

import java.util.Date;
import java.util.List;

public class Author {

	private String name;
	private String surnale;
	private String nationality;
	private Date bhirtday;
	private Date deathDate;
	private List<Opera> works;


	public Author(){
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurnale() {
		return surnale;
	}

	public void setSurnale(String surnale) {
		this.surnale = surnale;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Date getBhirtday() {
		return bhirtday;
	}

	public void setBhirtday(Date bhirtday) {
		this.bhirtday = bhirtday;
	}

	public Date getDeathDate() {
		return deathDate;
	}

	public void setDeathDate(Date deathDate) {
		this.deathDate = deathDate;
	}
	
	public List<Opera> getWorks() {
		return works;
	}

	public void setWorks(List<Opera> works) {
		this.works = works;
	}

	
}
