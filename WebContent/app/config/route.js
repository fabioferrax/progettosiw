var app = angular.module("app");

app.config(function ($routeProvider) {
	
	$routeProvider
	
	.when("/", {
		templateUrl: "views/login.html",
		controller : "loginController"
	})
	.when("/index", {
		templateUrl: "index.html",
		controller : "indexcontroller"
	})
	.when("/artists", {
		templateUrl: "views/artist.html",
		controller : "artistcontroller"
	})
	.otherwise({
		redirectTo: "/"
	});  
});